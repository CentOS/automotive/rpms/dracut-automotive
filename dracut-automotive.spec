%define dracutmodulesdir %{_prefix}/lib/dracut/modules.d

Name:          dracut-automotive
Version:       0.1
Release:       7%{?dist}
Summary:       A random collection of dracut scripts for automotive
License:       GPL-2.0-only
URL:           https://gitlab.com/CentOS/automotive/rpms/dracut-automotive
Source0:       module-setup.sh
Source1:       mount-sysroot.service

BuildRequires: systemd
Requires: dracut
Requires: systemd

%description
%{summary}.

%prep

%build

%install
install -D -m755 %{SOURCE0} ${RPM_BUILD_ROOT}/%{dracutmodulesdir}/01systemd-initrd-automotive/module-setup.sh
install -D -m644 %{SOURCE1} ${RPM_BUILD_ROOT}/%{_unitdir}/mount-sysroot.service

%files
%{dracutmodulesdir}/01systemd-initrd-automotive
%{_unitdir}/mount-sysroot.service

%changelog
* Mon Feb 3 2024 Enric Balletbo i Serra <eballetb@redhat.com> - 0.1-7
- Set action reboot on failure
* Wed Jun 19 2024 Eric Curtin <ecurtin@redhat.com> - 0.1-6
- Decrease restart interval from 2ms to 1ms
- Use kmsg+console
* Tue Jun 18 2024 Eric Curtin <ecurtin@redhat.com> - 0.1-5
- Decrease restart interval from 20ms to 2ms
- Use kmsg
* Mon Jun 17 2024 Eric Curtin <ecurtin@redhat.com> - 0.1-4
- Increase restart interval from 10ms to 20ms
* Mon Jun 17 2024 Eric Curtin <ecurtin@redhat.com> - 0.1-3
- Increase restart interval from 1ms to 10ms
* Thu May 23 2024 Eric Curtin <ecurtin@redhat.com> - 0.1-2
- Add more Before= things for image mode
* Wed May 22 2024 Eric Curtin <ecurtin@redhat.com> - 0.1-1
- Initial version

